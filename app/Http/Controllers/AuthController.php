<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        // return \App\User::all();
       
        if (! $token = auth()->attempt($credentials)) {

            
            return response()->json(['error' => 'Email or passsword doesnot exist'], 401);
        }
        
        $login=\App\userLogin::create([
            'useraccountId'=>auth()->user()->id,
            'loginOn'=>Carbon::now()
        ]);


        return $this->respondWithToken($token);
    }
     public function store(Request $request)
    {
        $user=\App\User::create($request->all());
        // return login($request);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $id=auth()->user()->id;
        auth()->logout();
        $logout=\App\userLogin::where('useraccountId','=',$id);
        $logout->loginOut=Carbon::now();
        $logout::save();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()->firstName,
            'userId'=>auth()->user()->id,
            'userType'=>auth()->user()->userType
           
        ]);
    }
}

