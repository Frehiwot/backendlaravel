<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class assignInstructor extends Controller
{
    public function listInstructor($departmentId)
    {
        $userr=\App\User::find($departmentId);

    	$user=\App\User::all()->where('userType','=','Instructor');
        // return $user;
        $instructor=$user->where('department','=',$userr->department);

        $i=0;
        $instructors=[];
        foreach($instructor as $inst)
        {
          
          $currentOfferingInstructors=\App\offeringInstructor::all()->where('year','=',date('Y'));
          $offeringInstructor=$currentOfferingInstructors->where('instructor','=',$inst->id);
          $instructors[$i]=$inst;
          $instructors[$i]->currentYearCourses=count($offeringInstructor);
          $i++;
        }
    	return $instructors;
    }
    public function store(Request $request,$departmentId)
    {
    	$offering=\App\offeringInstructor::create([
    		'instructor'=>$request->instructor,
    		'offering'=>$request->offering,
        'year'=>date('Y'),
    		'registeredBy'=>$departmentId,
    		'updatedBy'=>$departmentId
    	]);
      $offer=\App\courseOffering::find($request->offering);
    	return response()->json([
        'offering'=> $offering,
         'courseCode'=>$offer->course,
        'message'=>"Succesfull"
      ]);
    }
    public function listOffering($instructorId)
    {
        $instructor=\App\User::where('id','=',$instructorId)->first();
        $offering=\App\offeringInstructor::all()->where('instructor','=',$instructorId);
        $i=0;
        $cou=[];
       foreach($offering as $offer)
       {
         $course=\App\courseOffering::where('id','=',$offer->offering)->first();
         // return $course;
         $cou[$i]=$course;
         $i++;
       }

       return $cou;
  
        // return $offering;
        // return response()->json([
        //     'instructor'=> $instructor,
        //     'offering'=> $offering
            
        // ]);
    }
}
