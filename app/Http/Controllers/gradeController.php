<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class gradeController extends Controller
{
    public function listCourseGrade($studentId)
    {
    	return "list of courses with grade";
    }
    public function list($instructorId,$courseId)
    {

    }
    public function store($instructorId,$courseId,Request $request)
    {
    	return "store data";
    }
    public function update($instructorId,$courseId,Request $request)
    {
    	return "update data";
    }
    public function assignGrade($studentId)
    {
        // $offering=\App\courseOffering::where('course','=',$courseId)->first();
        // $semester=$offering->semester;
        $semester=\App\status::orderBy('semester')->get();
        $state=$semester->where('student','=',$studentId)->first();
        // $student=\App\status::all()->where('student','=',$studentId);
        // $state=$student->where('semester','=',$semester)->first();
        
        if(($state->totalCourseResult >= $state->totalCourseRegistration) || ($state->totalCourseResult === $state->totalCourseRegistration)){
            //calculating their cgpa
            $studentIdd=$state->student;
            $semesterId=$state->semester;
            $semester=\App\semester::find($semesterId);
            $offeredCourses=\App\courseOffering::all()->where('semester','=',$semester->semester);
            $courseResult=[];
            $i=0;
            
            foreach($offeredCourses as $offer)
            {

                 $specficCourse=\App\course::find($offer->course);
                 $registeredStudents=\App\courseRegistration::all()->where('student','=',$studentIdd);
                 $studentCourse=$registeredStudents->where('offering','=',$offer->id);
                 
                 $courseResult[$i]=$studentCourse;
                 $courseResult[$i]->ECTS=$specficCourse->ECTS;
                 $i++;
            }
            $j=0;
            foreach($courseResult as $result)
            {

                
                $grade=\App\grade::find($result[$j]->grade);
                
                $SGPA=0;
                if(($grade->name==="A+") || ($grade->name === "A"))
                {
                    // return "hello";
                    $SGPA+=($result->ECTS * 4);

                }
                elseif(($grade->name==="A-"))
                {
                    $SGPA+=($result->ECTS * 3.75);

                }
                elseif(($grade->name==="B+"))
                {
                    $SGPA+=($result->ECTS * 3.5);

                }
                elseif(($grade->name==="B"))
                {
                    $SGPA+=($result->ECTS * 3.0);

                }
                 elseif(($grade->name==="B-"))
                {
                    $SGPA+=($result->ECTS * 2.75);

                }
                 elseif(($grade->name==="C+"))
                {
                    $SGPA+=($result->ECTS * 2.5);

                }
                 elseif(($grade->name === "C"))
                {
                    $SGPA+=($result->ECTS * 2.0);

                }
                 elseif(($grade->name === "C-"))
                {
                    $SGPA+=($result->ECTS * 1.75);

                }
                 elseif(($grade->name === "D"))
                {
                    $SGPA+=($result->ECTS * 1.5);

                }
                  elseif(($grade->name === "F"))
                {
                    $SGPA+=($result->ECTS * 1.0);

                }
                $j++;


            }
            $state->SGPA=$SGPA/$state->totalCourseRegistration;
            $state->save();


        }
        return $state;
     
        
      
    }

}
