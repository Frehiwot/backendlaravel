<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class manageCourseController extends Controller
{
    public function list($departmentId)
    {
        $user=\App\User::find($departmentId);
        $courses=\App\course::all()->where('department','=',$user->department);
        $coursee=[];
        $i=0;
        foreach($courses as $course)
        {
          $coursee[$i]=$course;
          $i++;
        }
        return $coursee;
    }
    public function courseInformation($departmentId,$courseId)
    {
    	return "specfic course information";
    }
    public function store(Request $request,$departmentId)
    {
        

      try{
            $user=\App\User::find($departmentId);
            $course=\App\course::create([
                'code'=>$request->code,
                'title'=>$request->title,
                'module'=>$request->module[0]['moduleCode'],
                'lectureHours'=>$request->lectureHours,
                'laboratoryHours'=>$request->laboratoryHours,
                'creditHours'=>$request->creditHours,
                'ECTS'=>$request->ects,
                // 'registeredOn'=>date('y-m-d'),
                // 'updatedOn'=>date('y-m-d'),
                'department'=>$user->department,
                'registeredBy'=>$departmentId,
                'updatedBy'=>$departmentId
            ]);
            // $request->requisite !== "none" && $request->requisite !== null)
            if(count($request->requisite)>0)
            {
                foreach($request->requisite as $requisite)
                {
                    $requisite=\App\prerequisite::create([
                        'requiste'=> $requisite['code'],
                        'courseId'=>$request->code
                        
                    ]);
                    //please handle exception
                }
                return response()->json([
                        'course'=>$request->all(),
                        'message'=>'succesfull'
                        
                    ]);

                
            }
            else if(count($request->requisite)===0)
            {
                return response()->json([
                        'course'=>$request->all(),
                        'message'=>'succesfull'
                        
                    ]);
            }
        


            return $course;
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            // dd($e);
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);

        }
        catch(PDOException $e)
        {
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);
        }
    }
    public function update($id,Request $request,$departmentId)
    {
        $course=\App\course::find($id);
        $course->update($request->all());


    	return $course;
    }
    public function delete($departmentId,$courseId)
    {
        $course=\App\course::find($courseId);
        $course->delete();
    }
    public function listSpecficCourse($courseId)
    {
        $course=\App\course::where('id','=',$courseId)->first();
        $requisite=\App\prerequisite::all()->where('courseId','=',$course->id);
        
        return $course;
    }
}
