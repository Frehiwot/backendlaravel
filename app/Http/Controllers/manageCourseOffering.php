<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class manageCourseOffering extends Controller
{
    public function store(Request $request,$departmentId)
    {
    	
    	$offering=\App\courseOffering::create([
         'course'=>$request->course,
         'semester'=>$request->semester,
         'year'=>$request->year
         
    	]);

    	$course=\App\course::where('code','=',$request->course)->update(['offered' => 1]);
    
    	        
        return $offering;

    }
    public function listOffering($departmentId)
    {

        $user=\App\User::find($departmentId);

        $courses=\App\course::all()->where('department','=',$user->department); //retriving courses of the department
      
        $offers=[];
        $i=0;
        foreach($courses as $course)
        {
            $offering=\App\courseOffering::where('course','=',$course->code)->first();
            if(!empty($offering))
            {
                 $offers[$i]=$offering;
                  $i++;

            }
           
        }
       
        return $offers;
    }
    public function listSpecficOffering($offeringId)
    {

        $course=\App\courseOffering::where('id','=',$offeringId)->first();
        // $offering=\App\offeringInstructor::all()->where('offering','=',$offeringId);

        // return response()->json([
        //     'course' => $course,
        //     'offering'=>$offering
        // ]);
        
        return $course;

    }
    // public function list()
    // {
    //     $courseOffering=\App\offeringInstructor()::all()->where('offering','=',1);
        
    // }
}
