<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class manageSemester extends Controller
{
    public function store(Request $request,$departmentId)
    {
    	 $semester=\App\semester::create([
           'semester' => $request->semester,
           'year'=>$request->year,
           'startedOn'=>$request->startedOn,
           'endedOn'=>$request->endedOn,
           'registrationStarts'=>$request->registrationStarts,
           'registrationEnds'=>$request->registrationEnds
    		]);
       if(!empty($semester))
       {
        $semesterLoad=\App\semesterLoad::create([
          'semester'=>$semester->semester,
          'year'=>$semester->year,
          'semesterLoad'=>$request->semesterLoad
        ]);
       }
    	return response()->json([
    		'semester'=>$semester,
    		'message'=>'semester has registered succesfully'
    	]);

    }
    public function list()
    {
      $semesters=\App\semester::all();
      $semesterss=[];
      $i=0;
      foreach($semesters as $semester)
      {
        $semesterL=\App\semesterLoad::all()->where('semester','=',$semester->semester);
        $semesterLoad=$semesterL->where('year','=',$semester->year)->first();
        $semesterss[$i]=$semester;
        $semesterss[$i]->semesterLoad=$semesterLoad->semesterLoad;
        $i++;

      }
      return $semesterss;
    }
    public function update(Request $request)
    {
          $semester=\App\semester::find($id);
        $semester->update($request->all());


        return $semester;
    }
    public function delete($semesterId)
    {
       $semester=\App\semester::find($semesterId);
       $semester->delete();
    }
}
