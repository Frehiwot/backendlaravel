<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class moduleController extends Controller
{
    public function listModule($departmentId)
    {
    	$user=\App\User::find($departmentId);
    	return \App\module::all();
    }
    public function store(Request $request,$departmentId)
    {
    	 $module=\App\module::create([
           'moduleCode' => $request->moduleCode,
           'moduleTitle'=>$request->moduleTitle,
           'department'=>$request->department,
           
          
    		]);
       return $module;

    }
    
}
