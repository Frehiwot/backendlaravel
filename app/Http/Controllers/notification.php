<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class notification extends Controller
{
    public function retrieveNotification($userId)
    { 
    	$user=\App\user::find($userId);
    	$notification=$user->unreadNotifications;
    	return $notification;

    }
   
    public function read($userId,Request $request)
    {
    	$user=\App\user::find($userId);
        $user->unreadNotifications()->find($request->id)->markAsRead();
       return 'success';
    }

}
