<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\notificationn as NotificationsModel;

class offeringInstructor extends Controller
{
	public function list($instructorId)
	{
		$instructor=\App\offeringInstructor::all()->where('instructor','=',$instructorId);
        $offering=$instructor->where('year','=',date('Y'));

		$i=0;
		$offer=[];
		$courses=[];
		foreach($offering as $offers)
		{
			$course=\App\courseOffering::find($offers->offering);
			$offer[$i]=$course;
			$i++;
		}
        
		$j=0;
		foreach($offer as $offers)
		{
            $course=\App\course::where('code','=',$offers->course)->first();
            $courses[$j]=$course;
            $courses[$j]->offering=$offers->id;
            $j++;
		}
		return $courses;
	}
   	public function listStudents($offeringId)
	{

        //$offering=\App\courseOffering::where('course','=',$courseCode)->first();

        $registeredStudents=\App\courseRegistration::all()->where('offering','=',$offeringId);
        $students=[];
        $i=0;
       foreach($registeredStudents as $registered)
       {

       	 $studentData=\App\student::find($registered->student);
       	 $student=\App\User::where('id','=',$studentData->useraccountId)->first();
       	 $students[$i]=$student;
       	 $students[$i]->result=$registered->result;
       	 $i++;
         
       }

       return $students;

	}
    public function updatingStatus($studentId,$semester)
    {
        $student=\App\status::all()->where('student','=',$studentId);
        $state=$student->where('semester','=',$semester)->first();
        $totalCourseResult=$state->totalCourseResult;
        $state->totalCourseResult=$totalCourseResult+1;
        $state->save();

        return $state;
      
    }
    public function calculatingGrade($studentId,$semester)
    {
        
        $semester=\App\status::all()->where('semester','=',$semester);

        $student=\App\student::where('useraccountId','=',$studentId)->first();
        $state=$semester->where('student','=',$student->id)->last();

        $studentIdd=$state->student;
        $semesterId=$state->semester;
        $semester=\App\semester::where('semester','=',$semesterId)->first();
        $offeredCourses=\App\courseOffering::all()->where('semester','=',$semester->semester);
        $courseResult=[];
        $i=0;
        if(($state->CGPA!==null)&&($state->SGPA!==null))
        {
          return $state;
        }
            // return $offeredCourses;
            
            foreach($offeredCourses as $offer)
            {

                 $specficCourse=\App\course::where('code','=',$offer->course)->first();
               
                 if(!empty($specficCourse))
                 {
                    $currentYearRegistration=\App\courseRegistration::all()->where('year','=',date('Y'));
                    if(count($currentYearRegistration)>0)
                    {
                       $registeredStudents=$currentYearRegistration->where('student','=',$studentIdd);
                       if(count($registeredStudents)>0)
                       {
                           $studentCourse=$registeredStudents->where('offering','=',$offer->id)->first();
                           //return $specficCourse->ECTS;
                           if(!empty($studentCourse))
                           {
                               $courseResult[$i]=$studentCourse;
                               $courseResult[$i]->ECTS=$specficCourse->ECTS;
                               $courseResult[$i]->creditHours=$specficCourse->creditHours;
                               // echo $specficCourse;
                               $i++;
                           }
                           
                       }
                       
                    }
                     
                 }
                 
            }
            // return $courseResult[0];
            $j=0;
            // return $courseResult;
             $SGPA=0;
             $ECTS=0;
             $creditHours=0;
             if(count(\App\grade::all())===0)
             {
               return "Please enter grade data";
             }
            foreach($courseResult as $result)
            {
                

                $minvalue=\App\grade::where('minValue','<=',$result->result);
                $grade=$minvalue->where('maxValue','>=',$result->result)->first();
                // return $grade;
                // $grade=\App\grade::find($result[$j]->grade);
                $ECTS+=$result->ECTS;
                $creditHours+=$result->creditHours;
               
                if(($grade->name==="A+") || ($grade->name === "A"))
                {
                    // return "hello";
                    $SGPA+=($result->ECTS * 4);

                    //echo $SGPA;

                }
                elseif(($grade->name==="A-"))
                {
                    $SGPA+=($result->ECTS * 3.75);

                }
                elseif(($grade->name==="B+"))
                {
                    $SGPA+=($result->ECTS * 3.5);

                }
                elseif(($grade->name==="B"))
                {
                    $SGPA+=($result->ECTS * 3.0);

                }
                 elseif(($grade->name==="B-"))
                {
                    $SGPA+=($result->ECTS * 2.75);

                }
                 elseif(($grade->name==="C+"))
                {
                    $SGPA+=($result->ECTS * 2.5);

                }
                 elseif(($grade->name === "C"))
                {
                    $SGPA+=($result->ECTS * 2.0);

                }
                 elseif(($grade->name === "C-"))
                {
                    $SGPA+=($result->ECTS * 1.75);

                }
                 elseif(($grade->name === "D"))
                {
                    $SGPA+=($result->ECTS * 1.5);

                }
                  elseif(($grade->name === "F"))
                {
                    $SGPA+=($result->ECTS * 1.0);

                }
                // $j++;


            }
           // return $SGPA;
            // $state->SGP=$SGPA;
            $totalCreditHourss=$state->totalCreditHours;

            $totalEctss=$state->totalECTS;
            $state->totalCreditHours= $totalCreditHourss+$creditHours;
            $state->totalECTS=$totalEctss+$ECTS;
            // return $state->totalECTS;
            $state->SGPA=$SGPA/$ECTS;

            $state->save();
        $studentStatus=\App\Status::all()->where('student','=',$student->id);
        $i=0;
        $totalSGPA=0;
        $totalCGPA=0;

        $CGP=0;
        $CGPA=0;
        // return "hello";
        foreach($studentStatus as $status)
        {
           $totalSGPA+=$status->SGPA;
           $totalCGPA+=$status->CGPA;
           // $CGP+=$status->SGP;
           $i++;
        }
        $CGPA=$totalSGPA/count($studentStatus);
        
        $state->CGPA=$CGPA;
        $state->SGP=$totalSGPA;
        $state->save();
        // return $state->CGPA;
         $j=0;
         $studentStatuss=\App\Status::all()->where('student','=',$student->id);

        foreach($studentStatuss as $status)
        {
           
           $totalCGPA+=$status->CGPA;
           // $CGP+=$status->SGP;
           $j++;
        }
        $state->CGP=$totalCGPA;
        $state->save();
        
        $statusDescription=$this->determineAcadamicStatus($studentId,$state->SGPA,$state->CGPA,$semester,$student->currentYear,$state->id);
        $updateStudentYear=$this->updateStudentYear($state,$semester,$student->currentYear,$student->id);
        \App\User::find($studentId)->notify(new NotificationsModel($studentId,'Your grade report has been prepared'));
        return $state;
        
       // }
        // return $state;

    }
    public function determineAcadamicStatus($studentUserId,$SGPA,$CGPA,$semester,$year,$stateId)
    {
        
         $acadamicStatus=\App\acadamicStatuse::all()->where('year','=',$year);
        
         $semesterStatus=$acadamicStatus->where('semester','=',$semester->semester)->first();

         $semester=$semester->semester;
         if(!empty($semesterStatus))
         {
            if(($semester==1)&&($year==1))
            {
                if(($SGPA>=$semesterStatus->pass_starting_SGPA))
                 {
                      \App\studentStatus::create([
                        'statusId'=>$stateId,
                        'status'=>'PASS'
                      ]);
                      \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have passed for the current semester'));
                 }
                elseif(($SGPA>=$semesterStatus->probation_starting_SGPA)&& ($SGPA<=$semesterStatus->probation_ending_SGPA))
                {
                    \App\studentStatus::create([
                        'statusId'=>$stateId,
                        'status'=>'PASS with PROBATION/WARNING'
                      ]);
                     \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have passed with warning the current semester'));
                      
                }
                elseif(($SGPA>=$semesterStatus->dismissal_starting_SGPA)&& ($SGPA<=$semesterStatus->dismissal_ending_SGPA))
                {
                    \App\studentStatus::create([
                        'statusId'=>$stateId,
                        'status'=>'DISMISSED'
                      ]);
                     \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have been dismissed'));
                }
            
            }
            else
            {
                 if(($SGPA>=$semesterStatus->pass_starting_SGPA)&& ($CGPA>=$semesterStatus->pass_starting_CGPA))
                 {
                    \App\studentStatus::create([
                        'statusId'=>$stateId,
                        'status'=>'PASS'
                      ]);
                     \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have passed for the current semester'));
                  }
                 elseif((($SGPA>=$semesterStatus->probation_starting_SGPA) && ($SGPA<=$semesterStatus->probation_ending_SGPA))||(($CGPA>=$semesterStatus->probation_starting_CGPA)&& ($CGPA<=$semesterStatus->probation_ending_CGPA)))
                {
                    \App\studentStatus::create([
                        'statusId'=>$stateId,
                        'status'=>'PASS with PROBATION/WARNING'
                      ]);
                    \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have passed with warning the current semester'));

                }
                elseif(($SGPA>=$semesterStatus->dismissal_starting_SGPA)&& ($SGPA<=$semesterStatus->dismissal_ending_SGPA))
                {
                    if(($CGPA>=$semesterStatus->dismissal_starting_CGPA)&& ($CGPA<=$semesterStatus->dismissal_ending_CGPA))
                    {
                       \App\studentStatus::create([
                          'statusId'=>$stateId,
                          'status'=>'DISMISSED'
                        ]);
                        \App\User::find($studentUserId)->notify(new NotificationsModel($studentUserId,'Your have been dismissed'));
                    }
                    else
                    {
                        //check previous status
                    }

                }

            }
        } 
    }
    public function updateStudentYear($state,$semester,$currentYear,$studentId)
    {
        $orderedSemester=\App\semester::orderBy('startedOn')->get();
        $lastSemester=$orderedSemester->last();
        $statusMessage=\App\studentStatus::where('statusId','=',$state->id)->first();
        // if($semester == $lastSemester)
        // {
        //      if(!empty($statusMessage))
        //     {
        //         $checkStatus=$statusMessage->status;
        //         if(($checkStatus === 'PASS')||($checkStatus === 'PASS with PROBATION/WARNING'))
        //         {
        //             $student=\App\student::find($studentId);
        //             $studentYear=$student->currentYear;
        //             $student->currentYear=$studentYear+1;
        //             $student->save();

        //         }
        //     }
        //     //check graduation year

        // }
        if($semester->semester ==2)
        {
            if(!empty($statusMessage))
            {
                $checkStatus=$statusMessage->status;
                $checkGraduator=$this->checkGraduator($studentId,$student->currentYear);
                if($checkGraduator===false)
                {
                  if(($checkStatus === 'PASS')||($checkStatus === 'PASS with PROBATION/WARNING'))
                  {
                      $student=\App\student::find($studentId);
                      $studentYear=$student->currentYear;
                      $student->currentYear=$studentYear+1;
                      $student->save();

                  }

                }
                
            }

        }
    }
    public function checkGraduator($studentId,$studentYear)
    {
      $student=\App\student::find($studentId);
      $studentCurrentYear=$student->currentYear;
      $program=\App\program::where('programTitle','=',$student->programTitle)->first();
      if($programTitle->year === $studentCurrentYear)
      {
        return true;
      }
    }
    
	public function assignGrade($studentId,$courseId,Request $request)
	{
        $studentData=\App\student::where('useraccountId','=',$studentId)->first();
		$offering=\App\courseOffering::find($courseId);
        
        $semester=$offering->semester;
        $result=$request->all();
    
        $student=\App\courseRegistration::all()->where('student','=',$studentData->id);
        $courses=$student->where('offering','=',$courseId);
        $yearedCourses=$courses->where('year','=',date('Y'))->first();
        $yearedCourses->result=$result[0];
        
        $yearedCourses->save();

        
         $student=\App\User::where('id','=',$studentId)->first();
         
         $student->result=$yearedCourses->result;

         $state=$this->updatingStatus($studentData->id,$semester);
         

       if(($state->totalCourseResult >= $state->totalCourseRegistration) || ($state->totalCourseResult === $state->totalCourseRegistration))
        {

            $state=$this->calculatingGrade($studentId,$semester);


        }
      return $student;
      
	}
    
}
