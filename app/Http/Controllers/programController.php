<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class programController extends Controller
{
    public function listPrograms()
    {
    	return \App\program::all();
    }
    public function store(Request $request,$departmentId)
    {
    	$user=\App\user::find($departmentId);

    	$program=\App\program::create([
    		'programTitle'=>$request->programTitle,
    		'programDuration'=>$request->programDuration,
    		'studyLanguage'=>$request->studyLanguage,
    		'department'=>$user->department,
    		'credit'=>$request->credit,
    		'ECTS'=>$request->ects

         ]);
    	if($program)
    	{

           \App\programRequirment::create([
           	'programTitle'=>$request->programTitle,
           	'min_overall_credit'=>$request->min_overall_credit,
           	'max_overall_credit'=>$request->max_overall_credit,
           	'minimum_CGPA'=>$request->minimum_CGPA


           ]);
    	}
    	return $program;

    }
    public function listDepartmentPrograms($departmentId)
    {
    	$user=\App\user::find($departmentId);
    	return \App\program::all()->where('department','=',$user->department);

    }
}
