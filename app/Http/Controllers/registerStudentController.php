<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class registerStudentController extends Controller
{
	public function checkTimeTable()
	{
		$studentCourseRegistrationController=new studentCourseRegistrationController();
		$currentSemester=$studentCourseRegistrationController->currentSemester();
		if($currentSemester->semester==1)  //since most of the time registration of new students is available on semester 1 of the year
		{
    
				 return response()->json([
	                'message'=>'registration is available'
				]);
		
		}
		else
		{
			return response()->json([
                'message'=>'registration of new students is not available'
			]);
		}
		
	}
	public function store(Request $request,$id)
	{
		
	   try
       {
           $imageName = time().'.'.request()->photo->getClientOriginalExtension();
    			request()->photo->move(public_path('images'), $imageName);
    		
    		$user=\App\User::create([
        		'firstName'=> $request->firstName,
        		'middleName'=> $request->middleName,
        		'lastName'=>$request->lastName,
        		// 'sex' => $request->sex;
        		'primaryPhoneNumber'=> $request->phone,
                'secondaryPhoneNumber'=> $request->phone,
        		'email'=>$request->email,
        		'userId'=>$request->studentId,
        		'password'=>Hash::make($request->password),
        		'userType'=>'Student',
        		'department'=>$request->department,
        		
        		'registeredBy'=>$id,
        		'updatedBy'=>$id
        	]);
        

        	$student=\App\student::create([
        		'sex'=> $request->sex,
        		 'year-of-entry'=>$request->entry,
        		 'DOB'=>$request->DOB,
        		 'photo'=>$imageName,
        		 'programTitle'=>$request->program,
        		 'useraccountId'=>$user->id,
        		 'currentYear'=>1
        	]);

    		return response()->json([
    			'form'=>$user,
    			'message'=>'succesfully'
    		]);
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            // dd($e);
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);

        }
        catch(PDOException $e)
        {
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);
        }
	}
	public function list()
	{
		$student=\App\User::where('userType','=','Student')->get();
		// $student=\App\User::all();
		return $student;
	}

	public function specficStudent($id)
	{
		$student=\App\student::where('useraccountId','=',$id)->get();
		return $student;
	}
	 public function update(Request $request,$adminId,$id)
    {
          $user=\App\User::find($id);
        $user->update($request->all());


        return $user;
    }
    public function delete($userId)
    {
       $user=\App\User::find($userId);
       $student=\App\student::where('useraccountId','=',$userId);
       $user->delete();
       $student->delete();
    }
    public function sto(Request $request)
    {
    	// console.log($request->image);
    	$imageName = time().'.'.request()->image->getClientOriginalExtension();
    	return $imageName;
    }
    public function listCourses($studentId)
    {
        //check whther the student has registered
        //check the data
        //check their grade
        

        // return $offer;

        $user=\App\user::find($studentId);

        $student=\App\student::where('useraccountId','=',$user->id)->first();
        
        // return $student;
        // $student=\App\student::where('useraccountId','=',$user->id)->first();
        $year=$student['year-of-entry'];
        // return $year;
        $currentDate=date('Y-m-d H:i:s');

                // Declare and define two dates 
        $date1 = strtotime($year);
          
        $date2 = strtotime($currentDate);  
          
        // Formulate the Difference between two dates 
        $diff = abs($date2 - $date1);  
          
          
        // To get the year divide the resultant date into 
        // total seconds in a year (365*60*60*24) 
        $years = floor($diff / (365*60*60*24));  
          
          
        // To get the month, subtract it with years and 
        // divide the resultant date into 
        // total seconds in a month (30*60*60*24) 
        $months = floor(($diff - $years * 365*60*60*24) 
                                       / (30*60*60*24));  
          
          
       
        // $semester=\App\semester::latest('startedOn', 'desc')->get();
        $output = date('Y-m-d', strtotime($year));

        $yearOne=\App\semester::orderBy('startedOn')->get();

        // $yearOne=$semester->where('year','=',$years+1);

        
        $i=0;
        // print($output);
        // return $semester;

      
          
         $last=null;
         foreach($yearOne as $semesters)
          {
            
            if($i==0)
            {
                
                 
                // return $semesters->endedOn;
                // return $semesters;
                // echo "hello";
                if(($output <= $semesters->startedOn) || ($output >$semesters->startedOn&&$output<$semesters->endedOn))
                {
                    
                    // return $semesters->startedOn;
                    $startedOn=$semesters->semester;
                    // echo "hello2";
                    // echo $startedOn;
                    $registrationStarts=$semesters->registrationStarts;
                    $registrationEnds=$semesters->registrationEnds;
                    $i++;
                    // return $startedOn;
                }
                else{

                    $last=$semesters->endedOn;
                    // echo "in else";
                    $lastRegistration=$semesters->registrationEnds;
                    $i++;
                }
            }
            else{
                if(($output>$last && $output<= $semesters->startedOn)|| ($output >$semesters->startedOn&&$output<$semesters->endedOn))
                {

                    $startedOn=$semesters->semester;
                    // echo $semesters->registrationStarts;
                    $registrationStarts=$semesters->registrationStarts;
                    $registrationEnds=$semesters->registrationEnds;
                    
                }
                else{
                    $last=$semesters->endedOn;
                    $lastRegistration=$semesters->registrationEnds;
                     return response()->json([
                        
                        'message'=>'no valid semester'
                    ]);
                }
                $i++;
                
            }
            // return $startedOn;
           
          }
          // echo $output;
         
          // echo " ";
          // echo $registrationEnds;
        // $offer=[];
        // return $output;

       if( $output >= $registrationStarts && $output<=$registrationEnds)
       {
                  
            $courseOffering=\App\courseOffering::where('semester','=',$startedOn)->get();
            
            $i=0;
           
            foreach($courseOffering as $offering)
            {
                $course=\App\course::find($offering->course);
                
                if($course->department === $user->department)
                {
                     $offer[$i]=$course;
                }
               
                $i++;
               // return $offer;
            }
            
           
            $j=0;
            $k=0;
            $r=0;
            $registered=[];
            $cou=[];
            $status1=\App\status::all()->where('student','=',$studentId);

            $status2=$status1->where('semester','=',$startedOn)->first();

            if(!$status2)
            {
              \App\status::create([
             'student'=>$studentId,
             'semester'=>$startedOn,
             ]);
            }
            
            $student=\App\courseRegistration::all()->where('student','=',$studentId);


            if(count($student) >0)
            {

               foreach ($offer as $course) {

                    $offering=\App\courseOffering::where('course','=',$course->id)->first();

                    $taken=$student->where('offering','=',$offering->id)->first();
                    if($taken)
                    {
                        $registered[$j]=\App\course::find($course->id);
                        $j++;
                    }
                    if(!$taken){
                        $cou[$k]=\App\course::find($course->id);
                        $k++;
                    }
                }
                // return $registered;
                
                //only works if the student has registered for all courses previously
               
                if($j>0)
                {
                  // return $cou;
                    return response()->json([
                        'courses'=>$registered,
                        'cou'=>$cou,
                        'message'=>'already registered',
                        
                    ]);
                }
                else{
                    // return "not registered";
                      $co=[];
                     foreach ($offer as $course) {

                            
                            $co[$r]=\App\course::find($course->id);
                            $r++;
                        }
                        
                     return response()->json([
                        'courses'=>$co,
                        'message'=>'not registered'
                    ]);
                    
                }
           }
           else{
            // return "Not registered 2";
            $co=[];
             foreach ($offer as $course) {

                        
                        $co[$r]=\App\course::find($course->id);
                        $r++;
                    }
                
             return response()->json([
                        'courses'=>$co,
                        'message'=>'not registered'
                    ]);
                    
           }
           
     }
     else{
        return response()->json([
                    'courses'=>$offer,
                    'message'=>'registraton is not available'
                ]);

     }
        $offer=[];
    

   }
    
}
