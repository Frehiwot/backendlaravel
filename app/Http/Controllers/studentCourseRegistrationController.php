<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
class studentCourseRegistrationController extends Controller
{
	 public function currentStudentYear($studentId)
	 {
		//check whther the student has registered
		  //check the data
		  //check their grade
		  

		  // return $offer;

		  $user=\App\user::find($studentId);

		  $student=\App\student::where('useraccountId','=',$user->id)->first();
			$currentYear=$student->currentYear;
		  // return $student;
		  // $student=\App\student::where('useraccountId','=',$user->id)->first();
		  $year=$student['year-of-entry'];
		  // return $year;
		  
		  

		 

		  return $currentYear;
	 }
	 public function currentSemester()
	 {
		 $currentDate=date('Y-m-d');
		 // return $currentDate;
		 $semesters=\App\semester::orderBy('startedOn')->get();
		 // return $semesters;
		 $currentSemester=0;
		 $i=0;
		 foreach($semesters as $semester)
		 {
			if(($currentDate >= $semester->startedOn)&& ($currentDate <= $semester->endedOn))
			{
				 // return $semester;
				 $currentSemester=$semester;
				 $i++;
			}
		 }
		 return $currentSemester;
		 

	 }
	 public function canRegister($semester)
	 {
		 $currentDate=date('Y-m-d');

		 // $semester=$cur
		 if(($currentDate>=$semester->registrationStarts) && ($currentDate<=$semester->registrationEnds))
		 {
			 return 1;//can
		 }
		 else
		 {
			return 0;//can not
		 }
	 }
	  public function checkStatus($studentId,$currentSemester,$currentStudentYear)
	 {
	 	//we donot need to consider about the student currentSemester..
	 	$student=\App\student::where('useraccountId','=',$studentId)->first();
		$orderbyYear=\App\status::orderBy('year')->get();
		$status=$orderbyYear->where('student','=',$student->id);

		//return $status;
      
		if(count($status)>0)
		{
		  $lastStatus=$status->last();
          $studentStatus=\App\studentStatus::where('statusId','=',$lastStatus->id)->first();
		  if(!empty($studentStatus))
		  {
		  		$statusMessage=$studentStatus->status;
		  	    return $statusMessage;
		  }


		}
		else{
		  return 'beginner';
		}
		
	 }
	
	 public function semesterCourses($studentId,$semester,$currentYear)
	 {
 	
		 $user=\App\User::find($studentId);

		 $courses=\App\course::all()->where('department','=',$user->department);
		 $yearedCourses=\App\courseOffering::all()->where('year','=',$currentYear);
		 $semesterCourse=$yearedCourses->where('semester','=',$semester);
		 $courseOffering=[];

		 $i=0;
		 //retriving courses of the semester
		 foreach($courses as $course)
		 {
			  $offering=$semesterCourse->where('course','=',$course->code)->first();
			
			  if($offering)
			  {
				  	 $prerequisites=\App\prerequisite::all()->where('courseId','=',$course->code);

				  	 if(count($prerequisites)===0)
					  {
					  	 
							$module=\App\module::where('moduleCode','=',$course->module)->first();
							$courseOffering[$i]=$offering;
							$courseOffering[$i]->code=$course->code;
							$courseOffering[$i]->title=$course->title;
							$courseOffering[$i]->module=$module->moduleTitle;
							$courseOffering[$i]->creditHours=$course->creditHours;
							$courseOffering[$i]->ECTS=$course->ECTS;
							$courseOffering[$i]->message='no pre-requiste';
							$i++;
						  
			         }
		             elseif(count($prerequisites)>0)
		             {
		             	
		             	$checkPrerequisites=$this->checkPrerequisitesGrade($course,$prerequisites,$studentId);
		             	if($checkPrerequisites===true)
		             	{
		             		$module=\App\module::where('moduleCode','=',$course->module)->first();
							$courseOffering[$i]=$offering;
							$courseOffering[$i]->code=$course->code;
							$courseOffering[$i]->title=$course->title;
							$courseOffering[$i]->module=$module->moduleTitle;
							$courseOffering[$i]->creditHours=$course->creditHours;
							$courseOffering[$i]->ECTS=$course->ECTS;
							$courseOffering[$i]->message='pre-requistes condition fullfilled';
							$i++;

		             	}
		             	elseif($checkPrerequisites===false)
		             	{
		             		$module=\App\module::where('moduleCode','=',$course->module)->first();
							$courseOffering[$i]=$offering;
							$courseOffering[$i]->code=$course->code;
							$courseOffering[$i]->title=$course->title;
							$courseOffering[$i]->module=$module->moduleTitle;
							$courseOffering[$i]->creditHours=$course->creditHours;
							$courseOffering[$i]->ECTS=$course->ECTS;
							$courseOffering[$i]->message='pre-requistes condition not fullfilled';
							$i++;

		             	}
		            }
		        }
		    }
	 
		 return $courseOffering; 
	}
	public function checkCourseRepeatation($registeredOffering)  //$registered is the data for the registration
	{
		$studentCourseRegistrationCount=\App\courseRegistrationCount::all()->where('student','=',$registeredOffering->student);
        if(!empty($studentCourseRegistration))
        {
        	$courseRegistrationCount=$studentCourseRegistrationCount->where('offering','=',$registeredOffering->offering)->first();
        	if($courseRegistrationCount->count==1)
			{
				if($registeredOffering->result >=30 && $registeredOffering->result<=50)
				{
					
						return 1;
					// }
				}
			}
			elseif($courseRegistrationCount->count==2)
			{
				if($registeredOffering->result >=30 && $registeredOffering->result<=40)
				{
					
						return 2;
					// 	$i++;
					// }
				}

		}

        }
		

	}
	public function departmentSemesterOffering($semesterOffering,$department)
	{
		$courses=\App\course::all()->where('department','=',$department);
		$departmentSemesterOffering=[];
		$i=0;
		foreach($semesterOffering as $offerings)
		{
			$offeredCourse=$courses->where('code','=',$offerings->course)->first();
			if(!empty($offeredCourse))
			{
               $departmentSemesterOffering[$i]=$offeredCourse;
               $departmentSemesterOffering[$i]->offering=$offerings->id;
               $departmentSemesterOffering[$i]->semester=$offerings->semester;
               $departmentSemesterOffering[$i]->year=$offerings->year;
               $i++;
			}
		}
		return $departmentSemesterOffering;
	}

	public function registeredSemesterOffering($departmentSemesterOffering,$studentId,$currentYear)
	{
		$orderedCourseRegistration=\App\courseRegistration::orderBy('year','desc')->get();
		$courseRegistration=$orderedCourseRegistration->where('student','=',$studentId);
		$studentRegisteredSemesterOffering=[];
		$unregisteredSemesterOffering=[];
		$j=0;
		$i=0;
		if(!empty($courseRegistration))
		{
			foreach($departmentSemesterOffering as $semesterOffering)
			{
				$registeredSemesterOffering=$courseRegistration->where('offering','=',$semesterOffering->offering)->first();
				if(!empty($registeredSemesterOffering))
				{
					$studentRegisteredSemesterOffering[$i]=$registeredSemesterOffering;
					$i++;

				}
				elseif(empty($registeredSemesterOffering))
				{
					$unregisteredSemesterOffering[$j]=$semesterOffering;
					$j++;

				}

			}

		}
		$totalCourse[0]=$studentRegisteredSemesterOffering;
		$totalCourse[1]=$unregisteredSemesterOffering;

		return $totalCourse;
		
	}

	public function optionalSemesterCourses($studentId,$semester,$currentYear)
	{

		$student=\App\student::where('id','=',$studentId)->first();
		
		$user=\App\user::find($student->useraccountId);
		$semesterOffering=\App\courseOffering::all()->where('semester','=',$semester->semester);
        
		$departmentSemesterOffering=$this->departmentSemesterOffering($semesterOffering,$user->department);
		// return $departmentSemesterOffering;
        $bothRegisteredUnregisteredCourse=$this->registeredSemesterOffering($departmentSemesterOffering,$studentId,$currentYear);

        $studentRegisteredSemesterOffering=$bothRegisteredUnregisteredCourse[0];
         // return $studentRegisteredSemesterOffering;
        $studentUnregisteredSemesterOffering=$bothRegisteredUnregisteredCourse[1];
         // return $studentUnregisteredSemesterOffering;
		$orderedStatus=\App\Status::latest('year')->get();
		$studentStatus=$orderedStatus->where('student','=',$studentId)->first();
		$semesterCourseRegistration=[];
		
		if(!empty($studentStatus) && ($studentStatus->totalCourseRegistration===$studentStatus->totalCourseResult))
		{
			$statusMessage=\App\studentStatus::where('statusId','=',$studentStatus->id)->first();
			
			$i=0;
			if($studentStatus->CGPA<2.0 && $studentStatus->CGPA>1.8)
			{
				
				if(count($studentRegisteredSemesterOffering)>0)
				{
					
					foreach($studentRegisteredSemesterOffering as $registeredOffering)
					{
						$checkCourseRepeatation=$this->checkCourseRepeatation($registeredOffering);
						if($checkCourseRepeatation === 1)
						{
							$offering=\App\courseOffering::find($registeredOffering->offering);
						    $course=\App\course::where('code','=',$offering->course)->first();
							$semesterCourseRegistration[$i]=$course;
		                    $semesterCourseRegistration[$i]->message='this course is available,u can repeat this course';
							$i++;

						}
						elseif($checkCourseRepeatation === 2)
						{
							$offering=\App\courseOffering::find($registeredOffering->offering);
						
							$course=\App\course::where('code','=',$offering->course)->first();
							$semesterCourseRegistration[$i]=$course;
							$semesterCourseRegistration[$i]->message='u can repeat course for one last time';

						}
						
					}

				}
	        }
	        if($statusMessage->status == 'PASS')  //retreive courses the student havenot registered for
	        {
	        	//we also have to consider students who doesnot register previously,they nees to have pass status
	        	$j=count($semesterCourseRegistration);
	        	if(count($studentUnregisteredSemesterOffering)>0)
	        	{
	        		
	        		foreach($studentUnregisteredSemesterOffering as $unregisteredCourses)
		        	{
		        		if($unregisteredCourses->year < $currentYear)
		        		{
	                       $semesterCourseRegistration[$j]=$departmentOffering;
	                      $semesterCourseRegistration->message='u did not take this course on the previous semesters';
	                      $j++;
		        		}


		        	}
	        	}
	        	// $courseRegistrations=\App\courseRegistration::all()->where('student','=',$studentId);
	        	// $courseOffering=\App\courseOffering::all()->where('semester','=',$semester);
	        	// $takenCourseOfferings=[];
	        	// $untakenCourseOfferings=[];
	        	// $departmentCourseOffering=[];
	        	// $k=0;
	        	// $j=count($semesterCourseRegistration);
	        	// if($currentYear>1)
	        	// {
	        	// 	$previousYearCourseOfferings=$courseOffering->where('year','<',$currentYear);
	        	// 	$courses=\App\course::all()->where('department','=',$user->department); 
	        	// 	foreach($previousYearCourseOfferings as $yearCourses)
	        	// 	{
	        	// 		$departmentCourse=$courses->where('code','=',$yearCourses->course)->first();
	        	// 		if(!empty($departmentCourse))
	        	// 		{
	        	// 			// return $departmentCourse;
          //                  $departmentCourseOfferings[$k]=$departmentCourse;
          //                  $departmentCourseOfferings[$k]->offering=$yearCourses->offering;

          //                  $k++;
	        	// 		}
	        	// 	}
	        	// 	foreach($departmentCourseOfferings as $departmentOffering)
	        	// 	{
	        	// 		// return $departmentOffering;
	        	// 		$studentCourseRegistration=$courseRegistrations->where('offering','=',$departmentOffering['offering']);
	        	// 		if(empty($studentCourseRegistration))
	        	// 		{
          //                 $semesterCourseRegistration[$j]=$departmentOffering;
          //                 $semesterCourseRegistration->message='u did not take this course on the previous semesters';
          //                 $j++;

	        	// 		}
	        	// 	}

                  
	        	// }
	        }
	       
		}
		
		 return $semesterCourseRegistration;
    }
	public function listRegisteredCourses($studentId)
	{
		$student=\App\student::where('useraccountId','=',$studentId)->first();
		$registeredOffering=\App\courseRegistration::all()->where('student','=',$student->id);

		$registeredCourses=[];
		$i=0;

		foreach($registeredOffering as $registered)
		{
			$offering=\App\courseOffering::find($registered->offering);
			if(empty($offering))
			{
				return $registered;
			}
			$courses=\App\course::where('code','=',$offering->course)->first();
			$registeredCourses[$i]=$courses;
						$registeredCourses[$i]->result=$registered->result;
			$registeredCourses[$i]->semester=$offering->semester;
			$i++;

		}
		return $registeredCourses;


	} 

	 public function checkPrerequisitesGrade($course,$prerequisites,$studentId)
	 {
	 	$student=\App\student::where('useraccountId','=',$studentId)->first();
	 	$correctRequisite=[];
		$incorrectRequisite=[];
		$preRequisitesResult=[];
		$i=0;
		$j=0;
		$k=0;
		foreach($prerequisites as $prerequisite)
		{
			$requiredCourse=$prerequisite->requiste;
			$requiredCourseOffering=\App\courseOffering::where('course','=',$requiredCourse)->first();
			$requiredStudentCourseResult=\App\courseRegistration::where('student','=',$student->id);
			if(!empty($requiredStudentCourseResult))
			{
				// echo "course is".$requiredCourse."\n";
				// echo "coure registration".$requiredCourseOffering."\n";
				if(!empty($requiredCourseOffering))
				{
					$requiredCourseResult=$requiredStudentCourseResult->where('offering','=',$requiredCourseOffering->id);
					if(!empty($requiredCourseResult))
					{
						$orderbyYear=$requiredCourseResult->orderBy('year')->get();
						$orderedRequiredCourseResult=$orderbyYear->last();
						//
						$preRequisitesResult[$i]=$orderedRequiredCourseResult;
						$preRequisitesResult[$i]->courseCode=$prerequisite->requisite;
						$i++;
					}
				}
				
				
			}
			
        }
        if(count($preRequisitesResult)>0)
        {
        	foreach($preRequisitesResult as $results)
	        {
	        	//
	        	if($results->result>40)
	        	{
	        		$correctRequisite[$j]=$results;
	        		$j++;

	        	}
	        	else
	        	{
	        		$incorrectRequisite[$k]=$results;
	        		$k++;
	        	}
	        }

        }
        
        $originalLength=count($prerequisites);
        if(count($correctRequisite)===$originalLength)
        {
        	return true;
        }
        elseif(count($correctRequisite)<$originalLength)
        {
        	return false;
        }


	 }

	public function checkRegistration($semesterCourses,$studentId)
	{
		  $student=\App\student::where('useraccountId','=',$studentId)->first();
		  $courseRegistration=\App\courseRegistration::all()->where('student','=',$student->id);
		  $registeredCourse=[];
		  $i=0;
		  if(count($courseRegistration) >0)
		  {
				$registrationYear=$courseRegistration->where('year','=',date('Y'));

			   if(count($registrationYear)>0)
				{
				   $registered=$this->checkCurrentYearCourseRegistration($registrationYear,$semesterCourses);
				   // return $registered;
					if($registered)
					{
						return 'registered';
					}
					else
					{
						return 'not registered';
					}
				}
				else
				{
					$checkRegisteredCourses=$this->checkRegisteredCourses($semesterCourses,$courseRegistration);
				}
         }
		   else
		  {
			 return 'not registered';
		  }

	 }
	 public function checkCurrentYearCourseRegistration($currentYearRegisteredCourses,$semesterCourses)
	 {
			$registeredCourse=[];
			$i=0;
			 foreach($semesterCourses as $course)
			{
			  $offering=$currentYearRegisteredCourses->where('offering','=',$course->id)->first();

			  if(!empty($offering))
			  {
				  $registeredCourse[$i]=$course;
				  $i++;
			  }
			}
			if(count($registeredCourse) === count($semesterCourses))
			{
				return true;
			}
			elseif(count($registeredCourse)===0)
			{
                return false;
			}
			elseif(count($registeredCourse)>0)
			{
				return true;
			}

	 }
	 public function checkRegisteredCourses($semesterCourses,$courseRegistration)
	 {
		$registeredCourse=[];
		$i=0;
		foreach($semesterCourses as $course)
		{
			  $offering=$courseRegistration->where('offering','=',$course->id);

			  if(count($offering)>=2)
			  {
				 //consider course repeatation
				 $registeresCourse[$i]=$course;
			  }
			  elseif(count($offering)===1)
			  {
				 //check previous grade of this course to know whether or not he/she can repeat this course...consider course repetation 
			  }
			 
		}

	 }
	 public function listCoursesStudentregistered($semesterCourses,$studentId)
	 {
	 	$studentRegisteredCourses=[];
	 	$i=0;
	 	// return $semesterCourses;
	 	foreach($semesterCourses as $semesterCourse)
	 	{
	 		$registeredCourse=\App\courseRegistration::all()->where('offering','=',$semesterCourse->id);
	 		// return $studentId;
	 		if(count($registeredCourse)>0)
	 		{
	 			$studentRegisteredCourse=$registeredCourse->where('student','=',$studentId)->first();
	 			// return $studentRegisteredCourse;
		 		if(!empty($studentRegisteredCourse))
		 		{
	               $studentRegisteredCourses[$i]=$semesterCourse;
	               $i++;
		 		}

	 		}
	 		
	 	}
	 	return $studentRegisteredCourses;

	 }
	 public function listCourses($studentId)
	 {
		  $currentStudentYear=$this->currentStudentYear($studentId);
		  
		  $currentSemester=$this->currentSemester();
		  // return $currentSemester;
		  if(empty($currentSemester))
		  {
		  	//return all the homepage
		  	 return response()->json([
					
					'message'=>'Registration is not available',
					'specficMessage'=>'no current semester'
			  ]);

		  }
		 // return $currentSemester;
		  // return $currentSemester;
		  //checks registration date
		  $canRegister=$this->canRegister($currentSemester);
		  
		  // return $canRegister;
		  $checkStatus=$this->checkStatus($studentId,$currentSemester,$currentStudentYear);
		  //return $checkStatus;
		  if($checkStatus === "DISMISSED")
		  {
		  	 return response()->json([
					
					'message'=>'You have been dismissed,you can not register any more',
					'specficMessage'=>'no current semester'
			  ]);

		  }
		  // return "hello";
		  $semesterCourses=$this->semesterCourses($studentId,$currentSemester->semester,$currentStudentYear);
          //return $semesterCourses;
          
		  $user=\App\user::find($studentId);


		  $student=\App\student::where('useraccountId','=',$user->id)->first();
		  
		  // return $canRegister;
		  // return $optionalCourses;
		  // return $checkStatus;

		  if($canRegister == 1)
		  {
		  	//return "under can register";
		  	$optionalCourses=$this->optionalSemesterCourses($student->id,$currentSemester,$currentStudentYear);
			 if($checkStatus === 'beginner')  //this also has to consider many other issues
			 {
			     
				  $checkRegistration=$this->checkRegistration($semesterCourses,$studentId);
				  
				  if($checkRegistration === 'not registered')
				  {
						 return response()->json([
								'courses'=>$semesterCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'not registered'
						  ]);
				  }
				  if($checkRegistration === 'registered')
				  {
				  	    $studentRegisteredCourses=$this->listCoursesStudentregistered($semesterCourses,$student->id);
						 return response()->json([
								'courses'=>$studentRegisteredCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'already registered'
						  ]);
				  }


			 }
			 elseif(($checkStatus === 'PASS')||($checkStatus === 'PASS with PROBATION/WARNING'))
			 {
			 	
			 	  $checkRegistration=$this->checkRegistration($semesterCourses,$studentId);
			 	  // return $checkRegistration;
				  
				  if($checkRegistration === 'not registered')
				  {
						 return response()->json([
								'courses'=>$semesterCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'not registered'
						  ]);
				  }
				  if($checkRegistration === 'registered')
				  {
						 $studentRegisteredCourses=$this->listCoursesStudentregistered($semesterCourses,$student->id);
						 return response()->json([
								'courses'=>$studentRegisteredCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'already registered'
						  ]);
				  }

			 }
			 else
			 {
			 	$checkRegistration=$this->checkRegistration($semesterCourses,$studentId);
			 	if($checkRegistration === 'registered')
				  {
				  	    $studentRegisteredCourses=$this->listCoursesStudentregistered($semesterCourses,$student->id);
				  	    // return $studentRegisteredCourses;
						 return response()->json([
								'courses'=>$studentRegisteredCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'already registered'
						  ]);
				  }
				  else
				  {
				  	  return response()->json([
								'courses'=>$semesterCourses,
								'optionalSemesterCourses'=>$optionalCourses,
								'message'=>'not registered'
						  ]);
				  }
				  // }

			 }

		  }
		  elseif($canRegister == 0)
		  {

			  	if($checkStatus === 'beginner')  //this also has to consider many other issues
				 {
				     
					  $checkRegistration=$this->checkRegistration($semesterCourses,$studentId);
					  
					  if($checkRegistration === 'not registered')
					  {
							  return response()->json([
								'message'=>'registration is not available right now'

							 ]);
					  }
					  if($checkRegistration === 'registered')
					  {
							 return response()->json([
									'courses'=>$semesterCourses,
									
									'message'=>'already registered'
							  ]);
					  }


				 }
				 elseif(($checkStatus === 'PASS')||($checkStatus === 'PASS with PROBATION/WARNING'))
				 {
				 	
				 	  $checkRegistration=$this->checkRegistration($semesterCourses,$studentId);
				 	  // return $checkRegistration;
					  // echo 'hello world';
					  if($checkRegistration === 'not registered')
					  {
					  	    // echo $checkRegistration;
							 return response()->json([
									'message'=>'registration is not available right now'
							  ]);
					  }
					  if($checkRegistration === 'registered')
					  {
							 return response()->json([
									'courses'=>$semesterCourses,
									'optionalSemesterCourses'=>$optionalCourses,
									'message'=>'already registered'
							  ]);
					  }

			 }
			 // return response()->json([
				// 'message'=>'registration is not available right now'

			 // ]);
		  }
		 
		}



	 public function studentCourseRegister($offeringId,$studentId)
	 {
	 	 // $registeredOffering=\App\courseRegistrationCount::where('offering','=',$offeringId);
		  //$registrationCount=$registeredOffering->where('student','=',$studentId)->first();
	 	//if(empty($registrationCount))
	 	// {
	 	// 	$registration=\App\courseRegistration::create([
			//  'offering'=>$offeringId,
			//  'student'=>$studentId,
			//  'year'=>date('Y')
		 //  ]);
	 	// 	\App\courseRegistrationCount::create([
			// 	'student'=>$studentId
			// 	'count'=>1

			//  ]);

	 	// }
	 	// else
	 	// {
	 	// 	if($registrationCount['count']===1)
			//   {
			// 	 $count=$registrationCount->count+1;
			// 	 $registrationCount->count=$count;
			// 	 $registrationCount->save();
			//   }
			 

	 	// }
	 	 // $registeredCourses[$i]=$course;
		  $registration=\App\courseRegistration::create([
			 'offering'=>$offeringId,
			 'student'=>$studentId,
			 'year'=>date('Y')
		  ]);
		  $studentRegistrationCount=\App\courseRegistrationCount::all()->where('student','=',$studentId);
          if(count($studentRegistrationCount)>0)
		  {
		  	 $registrationCount=$studentRegistrationCount->where('offering','=',$offeringId)->first();
		  	 if(!empty($registrationCount))
		  	 {
                 if($registrationCount['count']===0)
				  {
					 \App\courseRegistrationCount::create([
						'registrationId'=>$registration->id,
						'count'=>1

					 ]);
				  }
				  elseif($registrationCount['count']===1)
				  {
					 $count=$registrationCount->count+1;
					 $registrationCount->count=$count;
					 $registrationCount->save();
				  }
		  	 }
		  	 elseif(empty($registrationCount))
		  	 {
		  	 	 \App\courseRegistrationCount::create([
					'student'=>$studentId,
					'offering'=>$offeringId,
					'count'=>1

				 ]);
		  	 }
		  	 

		  }
		  elseif(count($studentRegistrationCount)===0)
		  {
		  	 \App\courseRegistrationCount::create([
				'student'=>$studentId,
				'offering'=>$offeringId,
				'count'=>1

			 ]);

		  }
		  return $registration;
		 
		  // $i++;

	 }
	  public function storeStatus($studentId,$currentStudentYear,$currentSemester,$totalcourseregistration)
	 {

	 	 $state=\App\status::all()->where('student','=',$studentId);
		  if(count($state)>0)
		  {
				$statusYear=$state->where('year','=',$currentStudentYear);

				$statusSemester=$statusYear->where('semester','=',$currentSemester)->first();
				
				if(empty($statusYear))
				{
					  $status=\App\status::create([
						  'student'=>$studentId,
						  'semester'=>$currentSemester,
						  'year'=>$currentStudentYear,
						  'totalCourseRegistration'=>$totalcourseregistration
						]);
				}
				else
				{
					if(empty($statusSemester))
					{
						$status=\App\status::create([
						  'student'=>$studentId,
						  'semester'=>$currentSemester,
						  'year'=>$currentStudentYear,
						  'totalCourseRegistration'=>$totalcourseregistration
						]);
					}
					else
					{
						$statusSemester->totalCourseRegistration=$totalcourseregistration;
						$statusSemester->save();
					}

				}
		  }
		  else
		  {
				$status=\App\status::create([
				  'student'=>$studentId,
				  'semester'=>$currentSemester,
				  'year'=>$currentStudentYear,
				  'totalCourseRegistration'=>$totalcourseregistration
				]);
		  }

	 }

	 public function checkSemesterLoad($courses)
	 {
	 	$currentSemester=$this->currentSemester();
        $semesterLoad=\App\semesterLoad::where([['semester','=',$currentSemester->semester],['year','=',$currentSemester->year]])->first();
	 	$ECTS=0;
	 	foreach($courses as $course)
	 	{
	 		$ECTS+=$course['ECTS'];

	 	}
	 	// return response()->json([
	 	// 	'ECTS'=>$ECTS,
	 	// 	'semesterLoad'=>$semesterLoad

	 	// ]);
	 	if( $ECTS<=$semesterLoad->semesterLoad)
	 	{
	 		return true;
	 	}
	 	else
	 	{
	 		return false;
	 	}
	 }


	 public function courseRegistration(Request $request,$studentId)
	 {
	 	//we also have to check semester load
		 
		  $courses=$request->all();


		  $semesterLoad=$this->checkSemesterLoad($courses);
		  // return $semesterLoad;

		  if($semesterLoad === false)
		  {
		  	return response()->json([
                'cmd'=>'semester load error',
                //u have exceeded the semester load,please select only the courses u need
                'courses'=>$courses
		  	]);
		  }
		  $student=\App\student::where('useraccountId','=',$studentId)->first();
		  $i=0;
		  $registeredCourses=[];
		  $unregisteredCourses=[];
		  $i=0;
		  $j=0;
		 
		 foreach($courses as $course)
		 {

				
				$prerequisites=\App\prerequisite::all()->where('courseId','=',$course['code']);
				if(count($prerequisites)==0)
				{
					
					$registration=$this->studentCourseRegister($course['id'],$student->id);
					$registeredCourses[$i]=$course;
					 $i++;

				}
				elseif(count($prerequisites)>=1)
				{
				  $prerequisiteMessage=$course['message'];
				  if($prerequisiteMessage==='pre-requistes condition fullfilled')
				  {
				  	$registration=$this->studentCourseRegister($course['id'],$student->id);
				  	$registeredCourses[$i]=$course;
					 $i++;

				  }
				  elseif($prerequisiteMessage==='pre-requistes condition not fullfilled')
				  {
				  	$unregisteredCourses[$j]=$course;
				  	$j++;
				  }
				}
				
		}
		  //u have to consider semester
		 
		  $currentStudentYear=$this->currentStudentYear($studentId);
		  $currentSemester=$this->currentSemester();
		  $length=count($registeredCourses);
		  $this->storeStatus($student->id,$currentStudentYear,$currentSemester->semester,$length);
		  

		  if(count($unregisteredCourses)===0)
		  {
			 return response()->json([
				'registeredCourses'=> $registeredCourses,

				'cmd'=>'u have succesfully registered'
			  ]);
		  }
		  else
		  {
			  return response()->json([
				'registeredCourses'=> $registeredCourses,
				'unregisteredCourses'=>$unregisteredCourses,

				'cmd'=>'not succesfull'
			  ]);
		  }


		
	 }

	 public function gradeReport($studentId){
		  //check grade subnission data
		  $semester=App\status::orderBy('semester')->get();
		  $student=$semester->where('student','=',$studentId)->first();
			$offering=[];
			$courses=[];
		  if(($student->totalCourseRegistration === $student->totalCourseResult) || ($student->totalCourseRegistration <= $student->totalCourseResult))
		  {
			 //calculate sgpa and cgpa
				$student->SGPA=4.0;
				$student->CGPA=4.0;
				$student->state="pass";
				$student->save;
		  }
		  else
		  {
				$registration=\App\courseRegistration::where('student','=',$studentId);
				$registeredCourse=\App\courseOffering::where('semester','=',$student->semester);
				
				 $i=0;
				 $j=0;
				foreach($registration as $register)
				{ 
				  $offering[$i]=$registeredCourse->where('id','=',$register->offer)->first();
				  if($offering[$i])
				  {
					 $id=$offering[$i]->course;
					  $courses[$j]=\App\course::where('id','=',$id)->first();
					  $courses[$j]->result=$register->result;
					  $courses[$i]->grade=$register->grade;

				  }
				}


		  }
		  return $courses;



	 }

}
