<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;



class userManagementController extends Controller
{
    public function store(Request $request,$adminId)
    {
        $phone2=$request->phone2;
        if(empty($request->phone2))
        {
            $phone2=NULL;
        }
        else
        {
            $phone2=$request->phone2;
        }
        try{
            $user=\App\User::create([
            'firstName'=> $request->firstName,
            'middleName'=> $request->middleName,
            'lastName'=>$request->lastName,
            
            'primaryPhoneNumber'=> $request->phone,
            'secondaryPhoneNumber'=>$request->phone2,
            'email'=>$request->email,
            'userId'=>$request->userId,
            'password'=>Hash::make($request->password),
            'userType'=>$request->role,
            'department'=>$request->department,
            
            'registeredBy'=>1,
            'updatedBy'=>1
           ]);
             return response()->json([
                'user'=>$request->all(),
                'message'=>'succesfully'
            ]);
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            // dd($e);
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);

        }
        catch(PDOException $e)
        {
            return response()->json([
                'error'=>$e->errorInfo[2],
                'message'=>'error'
            ]);
        }
    	
        
        
    	
    }
    public function list($adminId)
    {
        try{
            return \App\User::all();
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            // dd($e);
            return response()->json([
                'error'=>$e->errorInfo[2],
                
            ]);

        }
        catch(PDOException $e)
        {
            return response()->json([
                'error'=>$e->errorInfo[2],
                
            ]);
        }
       
    }
    public function updateUser(Request $request,$id)
    {
        // return $request->all();
          $user=\App\User::find($request->id);

          
        $user->update($request->all());


        return $user;
    }
    public function deleteUser($userId)
    {
       $user=\App\User::find($userId);
       $user->delete();
    }
    public function specficUser($userId)
    {
        $user=\App\User::where('id','=',$userId)->first();
        return $user;
    }
}
