<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class acadamicStatuse extends Model
{
    protected $fillable=[
  'semester','year','pass_starting_SGPA','pass_starting_CGPA','probation_starting_SGPA','probation_ending_SGPA','probation_starting_CGPA','probation_ending_CGPA','dismissal_starting_SGPA','dismissal_ending_SGPA','dismissal_starting_CGPA','dismissal_ending_CGPA'
    ];
}
