<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{

    protected $fillable=[
    'code','title','lectureHours','laboratoryHours','creditHours','ECTS','registeredOn','updatedOn','registeredBy','updatedBy','department','module','offered'
    ];

    protected $primarykey='code';
}
