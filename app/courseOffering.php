<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courseOffering extends Model
{
    protected $fillable=[
    'course','semester','registeredBy','updatedBy','year'
    ];
}
