<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courseRegistrationCount extends Model
{
	// $table='course_registration_count';
    protected $fillable=[
     'student','offering','count'
    ];
    protected $table='course_registration_count';
}
