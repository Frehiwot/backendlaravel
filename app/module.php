<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class module extends Model
{
    // protected $primaryKey='moduleCode';
    protected $fillable=[
    'moduleCode','moduleTitle','department'
    ];
}
