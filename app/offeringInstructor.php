<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offeringInstructor extends Model
{
    protected $fillable=[
   'instructor','offering','registeredBy','updatedBy','year'
    ];
}
