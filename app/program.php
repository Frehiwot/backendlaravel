<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class program extends Model
{
    protected $fillable=[
    	'department','programTitle','programDuration','studyLanguage','ECTS','credit'
    ];
    public $timestamps = false;
}
