<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class programRequirment extends Model
{
    protected $fillable=[
    'programTitle','min_overall_credit','max_overall_credit','minimum_CGPA'
    ];
    protected $table='program__requirments';
}
