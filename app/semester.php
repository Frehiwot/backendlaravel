<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class semester extends Model
{
    protected $fillable=[
    	'semester','year','startedOn','endedOn','registrationStarts','registrationEnds'
    ];
}
