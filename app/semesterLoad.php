<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class semesterLoad extends Model
{
    protected $fillable=[
    	'semester','year','semesterLoad'
    ];
    public $timestamps = false;
}
