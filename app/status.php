<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class status extends Model
{
    protected $fillable=[
    'student','semester','year','state','SGPA','CGPA','totalCourseRegistration','totalCourseResult'
    ];
}
