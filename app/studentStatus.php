<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class studentStatus extends Model
{
    protected $fillable=[
    'statusId','status'
    ];
}
