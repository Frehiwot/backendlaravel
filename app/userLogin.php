<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userLogin extends Model
{
    protected $fillable=[
    'useraccountId','loginOn','loginOut'
    ];
}
