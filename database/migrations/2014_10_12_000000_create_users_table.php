<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
          
            $table->string('password');
            $table->string('firstName');
            $table->string('middleName');
            $table->string('lastName');
            // $table->string('sex');
            $table->string('primaryPhoneNumber')->unique();
            $table->string('secondaryPhoneNumber')->unique();
            $table->string('userId')->unique(); // string identification of the user like there id numberS
            $table->string('email')->unique();
           
            $table->timestamp('email_verified_at')->nullable();
            // $table->date('registeredOn');
            // $table->date('updatedOn');
            $table->Integer('registeredBy');
            $table->Integer('updatedBy');
            $table->string('department');
            $table->string('userType');
            $table->rememberToken();
            $table->timestamps();

            // $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
