<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->string('code');
            $table->string('title')->unique();
            $table->string('module');
            $table->Integer('lectureHours');
            $table->Integer('laboratoryHours');
            $table->Integer('creditHours');
            $table->Integer('ECTS');
            $table->string('department');
            $table->string('catagory')->default('Major');
            $table->integer('offered')->default(0);
            $table->BigInteger('registeredBy')->unsigned();
            $table->BigInteger('updatedBy')->unsigned();

            $table->timestamps();
            $table->primary('code');
            $table->foreign('module')->references('moduleCode')->on('modules')->onDelete('cascade');
            $table->foreign('registeredBY')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updatedBy')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
