<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->string('department');
            $table->string('studyLanguage');
            $table->string('programTitle');
            $table->integer('programDuration')->unsigned();
            $table->integer('credit')->unsigned();
            $table->integer('ECTS')->unsigned();
            $table->primary('programTitle');
            $table->foreign('department')->references('name')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
