<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramRequirmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program__requirments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('programTitle');
            $table->integer('min_overall_credit')->unsigned();
            $table->integer('max_overall_credit')->unsigned();
            $table->integer('minimum_CGPA')->unsigned();
            $table->boolean('internship')->default(false);
            $table->boolean('exitExam')->default(false);

            $table->foreign('programTitle')->references('programTitle')->on('programs')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program__requirments');
    }
}
