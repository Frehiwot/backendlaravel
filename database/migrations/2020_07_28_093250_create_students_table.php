<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('useraccountId')->unsigned()->unique();
            $table->date('DOB');
            $table->string('sex');
            $table->binary('photo');
            $table->date('year-of-entry');
            $table->string('programTitle');
            $table->timestamps();
            $table->foreign('useraccountId')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('programTitle')->references('programTitle')->on('programs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
