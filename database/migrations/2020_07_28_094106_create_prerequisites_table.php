<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrerequisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prerequisites', function (Blueprint $table) {
            $table->string('requiste'); //identification number of the course that become pre-requisite of another course
            $table->string('courseId'); 
            $table->primary(['requiste','courseId']);
            $table->foreign('requiste')->references('code')->on('courses')->onDelete('cascade');
            $table->foreign('courseId')->references('code')->on('courses')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prerequisites');
    }
}
