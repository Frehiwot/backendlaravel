<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semesters', function (Blueprint $table) {
            $table->integer('semester');
            $table->integer('year');
            $table->date('startedOn');
            $table->date('endedOn');
            $table->date('registrationStarts')->nullable();
            $table->date('registrationEnds')->nullable();
            $table->date('gradingStarts')->nullable();
            $table->date('gradingEnds')->nullable();
            $table->primary(['semester','year']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semesters');
    }
}
