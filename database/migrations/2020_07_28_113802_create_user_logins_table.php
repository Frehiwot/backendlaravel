<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_logins', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->BigInteger('useraccountId')->unsigned();
            $table->timestamp('loginOn')->default('2000-01-01 00:00:00');
            $table->timestamp('loginOut')->default('2000-01-01 00:00:00');
            $table->timestamps();

            $table->foreign('useraccountId')->references('id')->on('users')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logins');
    }
}
