<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseOfferingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_offerings', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('course'); // course identification number
            $table->integer('semester');
            $table->integer('year');  //year of the students like 1,2,3,4
            
            $table->timestamps();
            $table->foreign('course')->references('code')->on('courses')->onDelete('cascade');
            $table->foreign('semester')->references('semester')->on('semesters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_offerings');
    }
}
