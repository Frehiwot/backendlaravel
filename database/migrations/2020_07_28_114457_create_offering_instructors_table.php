<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferingInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offering_instructors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('instructor')->unsigned();
            $table->BigInteger('offering')->unsigned();
            $table->integer('year');
            $table->integer('registeredBy');
            $table->integer('updatedBy');
            $table->timestamps();
            $table->foreign('offering')->references('id')->on('course_offerings')->onDelete('cascade');
            $table->foreign('instructor')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offering_instructors');
    }
}
