<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_registrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('student')->unsigned();
            $table->BigInteger('offering')->unsigned();
            $table->integer('year');//only four integers like 2000
            $table->integer('result')->nullable();

            $table->foreign('student')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('offering')->references('id')->on('course_offerings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_registrations');
    }
}
