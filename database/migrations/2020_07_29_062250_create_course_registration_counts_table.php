<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseRegistrationCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_registration_counts', function (Blueprint $table) {
            $table->BigInteger('student')->unsigned();
            $table->string('offering');
            $table->unsignedInteger('count')->default(0);
            // $table->foreign(['student','offering'])->references(['student','offering'])->on('course_registrations'); 
            $table->primary(['student','offering']);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_registration_counts');
    }
}
