<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('student')->unsigned(); // make student and semester primary key
            $table->integer('semester');
            $table->integer('year'); // year like 2010
            $table->double('SGPA')->nullable();
            $table->double('CGPA')->nullable();
            $table->integer('totalCourseRegistration')->default(0); // count total number of the student registered for
            $table->integer('totalCourseResult')->default(0); // count total number of courses with result
            $table->foreign('student')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('semester')->references('semester')->on('semesters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
