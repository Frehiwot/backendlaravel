<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAcadamicStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_acadamic_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('statuseId')->unsigned();
            $table->BigInteger('acadamicStatuseId')->unsigned();
            $table->foreign('statuseId')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('acadamicStatuseId')->references('id')->on('acadamic_statuses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_acadamic_statuses');
    }
}
