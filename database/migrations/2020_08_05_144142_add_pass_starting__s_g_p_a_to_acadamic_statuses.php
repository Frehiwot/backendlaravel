<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPassStartingSGPAToAcadamicStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acadamic_statuses', function (Blueprint $table) {
            $table->integer('semester');
            $table->integer('year');  // year one or two(student year)
            $table->double('pass_starting_SGPA');
            $table->double('pass_starting_CGPA');
            $table->double('probation_starting_SGPA');
            $table->double('probation_ending_SGPA');
            $table->double('probation_starting_CGPA');
            $table->double('probation_ending_CGPA');
            $table->double('dismissal_starting_SGPA');
            $table->double('dismissal_ending_SGPA');
            $table->double('dismissal_starting_CGPA');
            $table->double('dismissal_ending_CGPA');
            $table->foreign('semester')->references('semester')->on('semesters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acadamic_statuses', function (Blueprint $table) {
            //
        });
    }
}
