<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemesterLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semester_loads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('semester');
            $table->integer('year');
            $table->unsignedInteger('semesterLoad')->default(0);
            $table->foreign(['semester','year'])->references(['semester','year'])->on('semesters')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semester_loads');
    }
}
