
<html><head>
<meta name="About the early chatbot called ELIZA" content="Eliza chatbot">
<title>Eliza, a chatbot therapist</title>
<base href="/">
 <script src="{{ asset('jquery.js') }}"></script>

<script language="JavaScript"><!-- 
// Chat Bot by George Dunlop, www.peccavi.com
// Note - Eliza is a Classic Model of chat Bots.. but this implementation is mine :)
// May be used/modified if credit line is retained (c) 1997 All rights reserved

    loaded = false;       // load flag for interlocking the pages

// OBJECT TYPE DEFINITIONS

// Keys

   maxKey = 36;
   keyNotFound = maxKey-1;
   keyword = new Array(maxKey);

  function key(key,idx,end){
    this.key = key;                     // phrase to match
      this.idx = idx;                     // first response to use
      this.end = end;                     // last response to use
      this.last = end;                // response used last time
    }
  maxrespnses =116;
   response = new Array(maxrespnses);

  maxConj = 19;
  max2ndConj = 7;
   var conj1 = new Array(maxConj);
   var conj2 = new Array(maxConj);
   var conj3 = new Array(max2ndConj);
   var conj4 = new Array(max2ndConj);
   

// Funtion to replaces all occurances of substring substr1 with substr2 within strng
// if type == 0 straight string replacement
// if type == 1 assumes padded strings and replaces whole words only
// if type == 2 non case sensitive assumes padded strings to compare whole word only
// if type == 3 non case sensitive straight string replacement

  var RPstrg = "";

  function replaceStr( strng, substr1, substr2, type){
    var pntr = -1; aString = strng;
    if( type == 0 ){  
      if( strng.indexOf( substr1 ) >= 0 ){ pntr = strng.indexOf( substr1 ); }
    } else if( type == 1 ){ 
      if( strng.indexOf( " "+ substr1 +" " ) >= 0 ){ pntr = strng.indexOf( " " + substr1 + " " ) + 1; } 
    } else if( type == 2 ){ 
      bstrng = strng.toUpperCase();
      bsubstr1 = substr1.toUpperCase();
      if( bstrng.indexOf( " "+ bsubstr1 +" " )>= 0 ){ pntr = bstrng.indexOf( " " + bsubstr1 + " " ) + 1; }  
    } else {
      bstrng = strng.toUpperCase();
      bsubstr1 = substr1.toUpperCase();
      if( bstrng.indexOf( bsubstr1 ) >= 0 ){ pntr = bstrng.indexOf( bsubstr1 ); } 
    }
    if( pntr >= 0 ){
      RPstrg += strng.substring( 0, pntr ) + substr2;
      aString = strng.substring(pntr + substr1.length, strng.length );
      if( aString.length > 0 ){ replaceStr( aString, substr1, substr2, type ); }
    }
    aString =  RPstrg + aString;
    RPstrg = "";
    return aString;
  } 


// Function to pad a string.. head, tail & punctuation

  punct = new Array(".", ",", "!", "?", ":", ";", "&", '"', "@", "#", "(", ")" )

  function padString(strng){
    aString = " " + strng + " ";
    for( i=0; i < punct.length; i++ ){
      aString = replaceStr( aString, punct[i], " " + punct[i] + " ", 0 );
    }
    return aString
  }

// Function to strip padding

  function unpadString(strng){
    aString = strng;
    aString = replaceStr( aString, "  ", " ", 0 );    // compress spaces
    if( strng.charAt( 0 ) == " " ){ aString = aString.substring(1, aString.length ); }
    if( strng.charAt( aString.length - 1 ) == " " ){ aString = aString.substring(0, aString.length - 1 ); }
    for( i=0; i < punct.length; i++ ){
      aString = replaceStr( aString, " " + punct[i], punct[i], 0 );
    }
    return aString
  }



// Dress Input formatting i.e leading & trailing spaces and tail punctuation
  
  var ht = 0;                       // head tail stearing
  
  function strTrim(strng){
    if(ht == 0){ loc = 0; }                 // head clip
    else { loc = strng.length - 1; }            // tail clip  ht = 1 
    if( strng.charAt( loc ) == " "){
      aString = strng.substring( - ( ht - 1 ), strng.length - ht);
      aString = strTrim(aString);
    } else { 
      var flg = false;
      for(i=0; i<=5; i++ ){ flg = flg || ( strng.charAt( loc ) == punct[i]); }
      if(flg){  
        aString = strng.substring( - ( ht - 1 ), strng.length - ht );
      } else { aString = strng; }
      if(aString != strng ){ strTrim(aString); }
    }
    if( ht ==0 ){ ht = 1; strTrim(aString); } 
    else { ht = 0; }    
    return aString;
  }

// adjust pronouns and verbs & such

  function conjugate( sStrg ){            // rephrases sString
    var sString = sStrg;
    for( i = 0; i < maxConj; i++ ){     // decompose
      sString = replaceStr( sString, conj1[i], "#@&" + i, 2 );
    }
    for( i = 0; i < maxConj; i++ ){     // recompose
      sString = replaceStr( sString, "#@&" + i, conj2[i], 2 );
    }
    // post process the resulting string
    for( i = 0; i < max2ndConj; i++ ){      // decompose
      sString = replaceStr( sString, conj3[i], "#@&" + i, 2 );
    }
    for( i = 0; i < max2ndConj; i++ ){      // recompose
      sString = replaceStr( sString, "#@&" + i, conj4[i], 2 );
    }
    return sString;
  }

// Build our response string
// get a random choice of response based on the key
// Then structure the response

  var pass = 0;
  var thisstr = "";
    
  function phrase( sString, keyidx ){
    idxmin = keyword[keyidx].idx;
    idrange = keyword[keyidx].end - idxmin + 1;
    choice = keyword[keyidx].idx + Math.floor( Math.random() * idrange );
    if( choice == keyword[keyidx].last && pass < 5 ){ 
      pass++; phrase(sString, keyidx ); 
    }
    keyword[keyidx].last = choice;
    var rTemp = response[choice];
    var tempt = rTemp.charAt( rTemp.length - 1 );
    if(( tempt == "*" ) || ( tempt == "@" )){
      var sTemp = padString(sString);
      var wTemp = sTemp.toUpperCase();
      var strpstr = wTemp.indexOf( " " + keyword[keyidx].key + " " );
      strpstr += keyword[ keyidx ].key.length + 1;
      thisstr = conjugate( sTemp.substring( strpstr, sTemp.length ) );
      thisstr = strTrim( unpadString(thisstr) )
      if( tempt == "*" ){
        sTemp = replaceStr( rTemp, "<*", " " + thisstr + "?", 0 );
      } else { sTemp = replaceStr( rTemp, "<@", " " + thisstr + ".", 0 );
      }
    } else sTemp = rTemp;
    return sTemp;
  }
  
// returns array index of first key found

    var keyid = 0;

  function testkey(wString){
    if( keyid < keyNotFound
      && !( wString.indexOf( " " + keyword[keyid].key + " ") >= 0 )){ 
      keyid++; testkey(wString); 
    }
  }
  function findkey(wString){ 
    keyid = 0;
    found = false;
    testkey(wString);
    if( keyid >= keyNotFound ){ keyid = keyNotFound; }
      return keyid;     
  }

// This is the entry point and the I/O of this code

  var wTopic = "";                      // Last worthy responce
  var sTopic = "";                      // Last worthy responce
  var greet = false;
  var wPrevious = "";                       // so we can check for repeats
  var started = false;  

  function listen(User){
      sInput = User;
    if(started){ clearTimeout(Rtimer); }
    Rtimer = setTimeout("wakeup()", 180000);    // wake up call
    started = true;                   // needed for Rtimer
    sInput = strTrim(sInput);             // dress input formating
    if( sInput != "" ){ 
      wInput = padString(sInput.toUpperCase()); // Work copy
      var foundkey = maxKey;                  // assume it's a repeat input
      if (wInput != wPrevious){             // check if user repeats himself
        foundkey = findkey(wInput);         // look for a keyword.
      }
      if( foundkey == keyNotFound ){
        if( !greet ){ greet = true; return "It is nice to be chatting with you." } 
        else {
          wPrevious = wInput;               // save input to check repeats
          if(( sInput.length < 10 ) && ( wTopic != "" ) && ( wTopic != wPrevious )){
            lTopic = conjugate( sTopic ); sTopic = ""; wTopic = "";
            return 'OK... "' + lTopic + '". Tell me more.';
          } else {
            if( sInput.length < 15 ){ 
              return "Tell me more..."; 
            } else { return phrase( sInput, foundkey ); }
          }
        }
      } else { 
        if( sInput.length > 12 ){ sTopic = sInput; wTopic = wInput; }
        greet = true; wPrevious = wInput;       // save input to check repeats
        return phrase( sInput, foundkey );      // Get our response
      }
    } else { return "I can't help, if you will not chat with me!"; }
  }
  function wakeup(){
      var strng1 = "    *** Are We going to Chat? ***";
      var strng2 = "  I can't help you without a dialog!";
      update(strng1,strng2);
  }
    
// build our data base here
                                 
    
  
    loaded = true;      // set the flag as load done
               
///////////////////////////////////////////////////////////////
//***********************************************************//
//* everything below here was originally in dia_1.html      *//
//***********************************************************//
///////////////////////////////////////////////////////////////

// Chat Bot by George Dunlop, www.peccavi.com
// May be used/modified if credit line is retained (c) 1997 All rights reserved

// Put together an array for the dialog
    
  chatmax = 5;            // number of lines / 2
  chatpoint = 0;
  chatter = new Array(chatmax);

// Wait function to allow our pieces to get here prior to starting

  function hello(){
    chatter[chatpoint] = "> Hello, I am your advisor.Please enter ur id to give more precise advising"; 
    chatpoint = 1;
    return write();
  }
  function start(){
    for( i = 0; i < chatmax; i++){ chatter[i] = ""; }
            console.log(chatpoint);
    chatter[chatpoint] = "  Loading...";
    document.Eliza.input.focus();
    write();      
    if( loaded ){ hello() }
    else { setTimeout("start()", 1000); }
  }

// Fake time thinking to allow for user self reflection
// And to give the illusion that some thinking is going on
  
  var elizaresponse = "";
  var student=-1;
  var Input="";
  
  function think(){
    document.Eliza.input.value = "";        
    if( elizaresponse != "" ){ respond(); }   
    else { setTimeout("think()", 250); }
  }
  function dialog(){
    console.log('hello');
    // if(chatpoint==0)
    // {
    //  start();
    // }
    // Input = document.Eliza.input.value;
    // console.log(Input);
    //    // capture input and copy to log
   //  document.Eliza.input.value = "";        
   //  chatter[chatpoint] = " \n* " + Input;
   //  $input=Input;
   //  console.log(chatpoint);
   //  "<?php echo ".Input."?>";
   //  console.log()
 
   //  if(chatpoint == 1)
   //  {
   //    console.log(Input);
      
   //    console.log('student id');
   //    console.log(student);
   //    elizaresponse="what can i help you?";
   //  }
   //  else
   //  {
   //    // elizaresponse = "<?php 
   //    // $student='<script> document.writeln(student)</script>';
   //    // $Input= '<script> document.writeln(Input)</script>'
   //    // echo `swipl -f C:/Users/User/Desktop/Advisor_ES_Copy.pl -g "advisor($student,$Input)." -t halt.`; ?>";

   //    elizaresponse = "<?php echo "hello"; ?>";

   //  }
    
   //  console.log("elizaResponse is");
   //  console.log(elizaresponse);
   //  setTimeout("think()", 1000 + Math.random()*3000);
   //  chatpoint ++ ; 
   //  if( chatpoint >= chatmax ){ chatpoint = 0; }
   //  return write();
    // Input = Inputt;   // capture input and copy to log
    // document.getElementById('input') = "enter";   
    // console.log(document.getElementById('input'));     
    // chatter[chatpoint] = " \n* " + Input;
    // $input=Input;
    // "<?php 
    // echo ".Input."?>";
 
    // if(chatpoint == 1)
    // {
    //   console.log(Input);
      
    //   console.log('student id');
    //   console.log(student);
    //   elizaresponse="what can i help you?";
    // }
    // else
    // {
    //   // elizaresponse = "<?php 
    //   // $student='<script> document.writeln(student)</script>';
    //   // $Input= '<script> document.writeln(Input)</script>'
    //   // echo `swipl -f C:/Users/User/Desktop/Advisor_ES_Copy.pl -g "advisor($student,$Input)." -t halt.`; ?>";

    //   elizaresponse = "<?php echo "hello"; ?>";

    // }
    
    // console.log("elizaResponse is");
    // console.log(elizaresponse);
    // setTimeout("think()", 1000 + Math.random()*3000);
    // chatpoint ++ ; 
    // if( chatpoint >= chatmax ){ chatpoint = 0; }
    // return write();
  }
  function respond(){
    chatpoint -- ; 
    if( chatpoint < 0 ){ chatpoint = chatmax-1; }
    chatter[chatpoint] += "\n> " + elizaresponse;
    chatpoint ++ ; 
    if( chatpoint >= chatmax ){ chatpoint = 0; }
    return write();
  }
// Allow for unprompted response from the engine

  function update(str1,str2){
    chatter[chatpoint] = " \n> " + str1;
    chatter[chatpoint] += "\n> " + str2;
    chatpoint ++ ; 
    if( chatpoint >= chatmax ){ chatpoint = 0; }
    return write();
  }

  function write(){
    document.Eliza.log.value = "";
    for(i = 0; i < chatmax; i++){
      n = chatpoint + i;
      if( n < chatmax ){ 
                // console.log("under n<chatmax in write function")
                document.Eliza.log.value += chatter[ n ]; 
                // console.log(chatter[n]);
            }
      else { document.Eliza.log.value += chatter[ n - chatmax ]; }
    }
    refresh();
    return false;                              // don't redraw the ELIZA's form!
  }
  function refresh(){ setTimeout("write()", 10000); }  // Correct user overwrites

  

// --></script>
<script >
  $(function()
  {
    $(".button").click(function()
    {
      var input=$("input#input");
      var dataString='input='+input;
      $.ajax({
        type:"POST",
        url:"<?php echo $_SERVER['PHP_SELF'];?>",
        data:dataString,
        success:function()
        {
          console.log("successful");
        }

      });
      return false;
    })
  })
</script>
 <?php
 $input='';
// echo "hello";
if(isset($_POST['submit']))
{
  $input=$_POST['input'];
  if(!isset($_GLOBAL['count']))
  {
    $_GLOBAL['count']=0;
  }
  else
  {
    $_GLOBAL['count']=$_GLOBAL['count']+1;
  }
  echo $_GLOBAL['count'];
  echo $input;
  
 //  $input=$_POST['input'];
 //  //echo $input;
 //  $chatpoint="<script>document.writeln(chatpoint);</script>";
 //  //echo $chatpoint;
 //  // $userId=$_GLOBAL['userId'];
 // //echo "userId".$userId;
 //  $response="what can we help u";

 //  if(($chatpoint== 1) || ($chatpoint==0))
 //  {
 //   // echo "inside";
 //   $userId=`swipl -f C:/Users/User/Desktop/Advisor_ES_Copy.pl -g "start('Nsr/4207/09',Id)." -t halt.`;
 //   $_GLOBAL['userId']=$userId;
 //   // echo $cmd;

 //  }
 //  else
 //  {
 //   $response=`swipl -f C:/Users/User/Desktop/Advisor_ES_Copy.pl -g "advisor($userId,$input)." -t halt.`;
  //}
  //echo $userId;
  //echo $response;
  // echo "<script>dialog(".$_GLOBAL['userId'].",".'what can we help u'.");</script>";
  // if($input === "start")
  // {
  //  echo "
   //  <script>
   //    window.onload=function()
    // {
    //  dialog();
    // }
     
   //  </script>
   //  ";

  // }
  // elseif($input==="hello")
  // {
    echo "<script>dialog();</script>";
  // }
  
  // echo "<br/>";

}
?>
<link rel="stylesheet" href="http://www.manifestation.com/station.css">
</head>
<body onload="start();" bgcolor="#FFFFFF">



<h1>ELIZA: a very basic Rogerian psychotherapist chatbot</h1>

<br>
 
<div style="float:left; margin-right:10px;">
<script type="text/javascript"><!--
google_ad_client = "pub-5782868064860207";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
google_ad_channel ="";
google_color_border = "000000";
google_color_bg = "EEEEEE";
google_color_link = "0033FF";
google_color_url = "0066CC";
google_color_text = "000000";
//--></script>
<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
<p>
  
</p>
<table cellspacing="0" cellpadding="2" border="0" align="center" class="neurotoy" >
  <tbody><tr><th bgcolor="#ffffff" align="left">
    <font color="#00000">Talk to Eliza by typing your questions and answers in the input box.</font>
  </th></tr>
    <tr><td bgcolor="#0097FF">
      <div id="contact_form">
        <form name="Eliza" method="post" action="" >
          <textarea rows="18" cols="75" name="log"></textarea>
          <center><font color="#000000"><br>TYPE HERE</font>
        <input type="text" size="65" name="input" id="input" value="<?php echo $input ?>">
        <input type="submit" name="submit" class="submit" id="submit_btn" value="submit" ></center>
        </form>
        </div>
    </td></tr>
</tbody></table>

<hr>
<p>
  <!-- text contents by Ken Ronkowitz  ronkowitz.com last modified 2018  -->
  
I first encountered ELIZA on the Tandy/Radio Shack computers that made up the first computer lab in the junior high school where I taught in the 1970s. By then, ELIZA was a software tween herself.</p>
<p>This early natural language processing  program had been written in the mid-1960s at the MIT Artificial Intelligence Laboratory by Joseph Weizenbaum. It supposedly had been created  to demonstrate how superficial human to computer communications was at that time. But, when it was put on personal computers, humans found it quite engaging.</p>
<p>WHAT DOES ELIZA DO?<br>
Using "'pattern matching" and substitution methodology, the program gives canned responses that made early users feel they were talking to someone who understood their input. The program was limited by the scripts that were in the program. (ELIZA was originally written in <a href="https://en.wikipedia.org/wiki/MAD_(programming_language)" title="about" target="_blank">MAD-Slip</a>.) Many variations on the original scripts were made as amateur coders played around with the fairly simple code. 
</p><p>Perhaps the most well known variation was called DOCTOR. This was made to respond like a<a href="https://en.wikipedia.org/wiki/Person-centered_therapy" title="about" target="_blank"> Rogerian psychotherapis</a>t. In this instance, the therapist "reflects" on questions by turning the questions back at the patient.</p>

  <p>ELIZA was one of the first chatterbots (later clipped to chatbot). It was also an early test case for the Turing Test, a test of a machine's ability to exhibit intelligent behavior equivalent to, or indistinguishable from, that of a human. By today's standards ELIZA fails very quickly if you ask it a few complex questions. 
  </p>
<p></p><center><img src="images/sexting.jpg" width="300" height="205" alt="classic"></center><p></p>
  <p>That said, ELIZA delighted my students, and those who were in my little programming club at that time were also delighted to make their own versions by revising and adding to the code and scripts.</p> 
  <p>One particularly memorable moment was when a student asked "What happens if someone types in a curse word?   "<br> 
    I said, "She will respond based on some word you use, but she won't know those kinds of words."<br> 
    They tried it out. Someone would tell ELIZA to do something obscene and she might answer "We were discussing you, not me," which is actually pretty clever and funny - by accident.<br>
"Can we fix that?" they asked.<br>
"Sure. But you'll need to type in every possible curse word so that she knows what to respond to."<br>
"We can do that?" they asked rather incredulously.<br>
"You would have to if you want her to really respond logically, " I concluded. They were delighted and set to work immediately. Luckily, I never received any parent phone calls about it. Our little secret. 
</p><p></p><center><img src="images/sheldon radio shack.jpg" width="300" height="168" alt=""> </center><p></p>       
  <p>The version of ELIZA below is a more recent javascript incarnation to which I have made some cosmetic and scripresponset changes. She is still a baby chatbot, but she has had a 2018 resurgence of interest because she was featured in an episode of the TV show<em> Young Sheldon</em>. (The episode, "A Computer, a Plastic Pony, and a Case of Beer," may still be available at<a href="http://www.cbs.com/shows/young-sheldon/video/S_ft1wDeyvydHoQMizE7iablVG6DDoe6/young-sheldon-a-computer-a-plastic-pony-and-a-case-of-beer/" title="CBS" target="_blank"> www.cbs.com</a>) Sheldon and his family become quite enamored by ELIZA, though the precocious Sheldon quickly realizes it is a limited program.
</p><p>Give ELIZA a try. You can sit on your own couch and pretend it is a therapist's couch. And, as with Siri, Alexa and other operating system disembodied voices, feel free to conjure up your own idea of what ELIZA looks like.  </p>
<br>
<br><br>

<br><center><img class="size-full aligncenter" src="https://cdn-images-1.medium.com/max/1600/1*QkuCSWmttLPT_5-2YyqklQ.jpeg" width="800"></center>
<br>
<br>

<p align="center">This javascript version of ELIZA was originally written by Michael Wallace and enhanced by George Dunlop.</p> 
</body></html>
 



