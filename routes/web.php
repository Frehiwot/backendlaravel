<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/pro/{message}','prolog@pro');
// ManageCourse 

Route::get('images/{filename}', function ($filename)
{
	$path = storage_public('images/' . $filename);

   

    if (!File::exists($path)) {

        abort(404);

    }

  

    $file = File::get($path);

    $type = File::mimeType($path);

  

    $response = Response::make($file, 200);

    $response->header("Content-Type", $type);

 

    return $response;
    
});

Route::get('/chat','prolog@index');
Route::post('departmentHead/{departmentId}/register/program','programController@store');

// Route::get('/courses','manageCourseController@list');
Route::get('/departmentHead/{departmentHeadId}/courses','manageCourseController@list');
Route::get('/courses/registered/{studentId}','studentCourseRegistrationController@listRegisteredCourses');
// Route::get('/departmentHead/{departmentHeadId}/course/{courseId}','manageCourseController@courseInformation');

// Route::post('/storeCourse','manageCourseController@store');
Route::post('/departmentHead/{departmentHeadId}/storeCourse','manageCourseController@store');


Route::post('/register','registerStudentController@sto');
// Route::put('/updateCourse/{id}','manageCourseController@update');
Route::put('/departmentHead/{departmentHeadId}/course/{courseId}','manageCourseController@update');

Route::delete('/departmentHead/{departmentHeadId}/course/{courseId}','manageCourseController@delete');


Route::post('/admin/{adminId}/registerUser','userManagementController@store');
Route::get('/admin/{adminId}/Users','userManagementController@list');
Route::put('/admin/{adminId}/user/{id}','userManagementController@updateUser');
Route::get('/admin/user/{id}','userManagementController@deleteUser');



Route::post('/departmentHead/{departmentHeadId}/storeOffering','manageCourseOffering@store');
Route::get('/departmentHead/{departmentHeadId}/listOffering','manageCourseOffering@listOffering');
Route::get('/departmentHead/{departmentHeadId}/listInstructors','assignInstructor@listInstructor');
Route::get('/departmentHead/instructorOffering/{instructorId}','assignInstructor@listOffering');

Route::post('/departmentHead/{departmentHeadId}/storeOfferingInstructors','assignInstructor@store');


Route::get('/courseOffering/{offeringId}','manageCourseOffering@listSpecficOffering');
Route::get('/course/{courseId}','manageCourseController@listSpecficCourse');
Route::get('/user/{userId}','userManagementController@specficUser');
// Route::post('/departmentHead/{departmentHeadId}/course','assignInstructor@store');

Route::get('/registralClerk/{registeredBy}/Semesters','manageSemester@list');
Route::post('/registralClerk/{registeredBy}/registerSemester','manageSemester@store');
Route::put(' /registralClerk/updateSemester/{id}','manageSemester@update');
Route::delete('/registralClerk/deleteSemester/{id}','manageSemester@delete');

Route::get('/registralClerk/Students','registerStudentController@list');
Route::get('/registralClerk/registerStudent','registerStudentController@checkTimeTable');
Route::post('/registralClerk/{registeredBy}/registerStudent','registerStudentController@store');
Route::put(' /registralClerk/updateStudent/{id}','registerStudentController@update');
Route::get('/registralClerk/deleteStudent/{id}','registerStudentController@delete');
Route::get('/registralClerk/Student/{id}','registerStudentController@specficStudent');



Route::get('/student/{studentId}/courses','studentCourseRegistrationController@listCourses');


// //registerStudentController
Route::post('/registralClerk/registerStudent','registerStudentController@store');

Route::post('/student/{studentId}/courses','studentCourseRegistrationController@courseRegistration');

Route::get('/student/{studentId}','studentCourseRegistrationController@listt');


Route::get('/student/{studentId}/currentYear','studentCourseRegistrationController@currentStudentYear');
Route::get('/student/currentSemester','studentCourseRegistrationController@currentSemester');

// Route::get('/student/{currentSemester}/{currentYear}/{studentId}','studentCourseRegistrationController@semesterCourses');
// Route::get('/student/{currentSemester}/{currentYear}/{studentId}','studentCourseRegistrationController@optionalSemesterCourses');

Route::get('/prolog','studentCourseRegistrationController@list');


Route::get('/instructor/courses/{instructorId}','offeringInstructor@list');
Route::get('/course/{courseId}/students','offeringInstructor@listStudents');


Route::put('/student/{studentId}/course/{courseId}/result','offeringInstructor@assignGrade');
Route::get('/student/{studentId}/{semester}','offeringInstructor@calculatingGrade');
// Route::get('/student/{studentId}/gradee/{state}','offeringInstructor@calculatingGrade');


Route::get('/registralClerk/student/programs','programController@listPrograms');

Route::get('/departmentHead/{departmentHead}/modules','moduleController@listModule');

Route::get('/{studentId}/{currentStudentYear}/{currentSemester}/{totalcourseregistration}','studentCourseRegistrationController@storeStatus');

Route::get('/notifications/{studentId}/student','notification@retrieveNotification');

Route::get('/list/departments','departmentController@listDepartments');

Route::post('/departmentHead/{departmentId}/register/module','moduleController@store');

Route::get('departmentHead/{departmentId}/programs','programController@listDepartmentPrograms');














